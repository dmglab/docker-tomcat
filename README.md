# Centos7 based tomcat installation

WIP! This is just running the sample apps.

Inspired by [tutum/tomcat][1]

## Usage

`docker run -p 8080:8080 -d dmglab/tomcat`

To enter the **admin** interface, take a look into the logs   
`docker logs $(docker ps -l -q)`

you should find something like this

    => Creating and admin user with a random password in Tomcat
    => Done!
    ========================================================================
    You can now configure to this Tomcat server using:
    
        admin:Qax5PGWyui8g
    
    ========================================================================



  [1]: https://github.com/tutumcloud/tutum-docker-tomcat