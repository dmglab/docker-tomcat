FROM dmglab/centos

RUN yum install -y tomcat tomcat-webapps tomcat-admin-webapps

EXPOSE 8080

ADD run.sh /usr/local/tomcat/

RUN chmod +x /usr/local/tomcat/run.sh
CMD ["bash", "-c", "/usr/local/tomcat/run.sh"]
